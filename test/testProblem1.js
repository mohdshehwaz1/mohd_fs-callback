const fs = require('fs');
const path = require('path')

const problem1 = require('../problem1')

problem1.createRandomJSONFiles((randomFilePath)=>{
    fs.writeFile(randomFilePath,JSON.stringify(fs.readFileSync('../lipsum.txt','utf-8')),(err,data)=>{
        if(err){
            console.log("Error occured")
        }
        else{
            console.log("File created successfully")
        }
    })
})

const folderPath = '../randomDirectory'




problem1.deleteJSONFiles(fs.readdirSync(folderPath),(pathOfEachFile)=>{
    fs.unlink(pathOfEachFile,(err,data)=>{
        if(err){
            console.log(err.message)
        }
        else{
            console.log("All files deleted")
        }
    })
})