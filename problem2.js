const fs = require('fs')
const path = require('path')
const { createRandomJSONFiles } = require('./problem1')



function readAndWriteAndDeleteFile(lipsumFilePath){
    returnReadFile(lipsumFilePath,(err,data)=>{
        if(err){
            console.log(err)
        }
        else{
            writeFilesForSpecificProblem(data.toUpperCase(),()=>{
                const filenamesPath=path.join(__dirname,'randomDirectory','filenames.txt')
                returnReadFile(filenamesPath,(err,upperCaseData)=>{
                    if(err){
                        console.log(err)
                    }
                    else{
                        writeFilesForSpecificProblem(upperCaseData.toLowerCase(),(err)=>{
                            const filenamesPath=path.join(__dirname,'randomDirectory','filenames.txt')
                            returnReadFile(filenamesPath,(err,lowercaseData)=>{
                                if(err){
                                    console.log(err)
                                }
                                else{
                                    let splitData=lowercaseData.split(/[,.'\n\']/)
                                    writeFilesForSpecificProblem(JSON.stringify(splitData.sort()),() =>{
                                        const filenamesPath=path.join(__dirname,'randomDirectory','filenames.txt')
                                        deleteFiles(filenamesPath)
                                    })

                                }
                                })
                            
                        })
                    }
                })

            })
        }
    })
}



function returnReadFile(lipsumFilePath,cb){
    fs.readFile(lipsumFilePath,'utf-8',(err,data)=>{
        if(err){
            console.log(err)
        }
        else{
            
            cb(err,data)
        }
    })

}

function writeFilesForSpecificProblem(data , cb) {
   
    fs.writeFile('../randomDirectory/filenames.txt', data, (err) => {
        if(err){
            console.log(err)
        }
        cb()
    })
}

function deleteFiles(filenamesPath){
    fs.unlink(filenamesPath,(err,data)=>{
        if(err){
            console.log(err)
        }
        else{
            console.log("All files deleted")
        }
    })
}



module.exports = readAndWriteAndDeleteFile