
const { fstat } = require('fs')
const path = require('path')

//Random Files creation
const totalFilesName=[]

function createRandomJSONFiles(createJsonFiles){
    try{
        let randomNameOfFiles='file'
        for(index=0;index<5;index++){
            const randomFilePath=path.join(__dirname,'randomDirectory',randomNameOfFiles.concat(index,'.json'))
            totalFilesName.push(path.basename(randomFilePath))
            createJsonFiles(randomFilePath)

        }
        

    }
    catch(err){
        console.log(err)
    }
}



//Delete RandomFiles

function deleteJSONFiles(totalFiles,deleteFiles){
    try{
        for(index=0;index<totalFiles.length;index++){
            const pathOfEachFile=path.join(__dirname,'randomDirectory',totalFiles[index])
            deleteFiles(pathOfEachFile)
        }
    }
    catch(err){
        console.log(err.message)
    }
    

}

module.exports = {
    createRandomJSONFiles,
    deleteJSONFiles
}